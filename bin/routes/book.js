const express = require('express');
const Book = require('../models/Book');
const app = express();

app.post('/book/create', (req, res) => {
  let {title, pages, year, isbn, author} = req.body;
  let libro = new Book(title, pages, year, isbn, author);
  let created = libro.create(libro);

  res.json({status: true, message: created});
});

app.get('/book/list', (req, res) =>{
  let libro = new Book();
  let lista = libro.find();

  res.json({status: true, listaLibros: lista});
});

app.get('/book/:isbn', (req, res) =>{
  let libro = new Book();
  let isbn = req.params.isbn;
  let bookByIsbn = libro.findByISBN(isbn);

  res.json({status: true, book: bookByIsbn});
});

app.delete('/book/delete/:isbn', (req, res) =>{
  let libro = new Book();
  let isbn = req.params.isbn;
  let message = libro.deleteByISBN(isbn);
  
  res.json({status: true, message: message});
});

app.put('/book/update/:isbn', (req, res) =>{
  let {title, pages, year, author} = req.body;
  let isbn = req.params.isbn;
  let libro = new Book(title, pages, year, isbn, author);
  let updated = libro.update(isbn, libro);
  
  res.json({status: true, message: updated});
});

module.exports = app;