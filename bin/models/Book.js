const fs = require('fs');
let listBooks = [];

class Book{
  constructor(title, pages, year, isbn, author){
    this.title = title;
    this.pages = pages;
    this.year = year;
    this.isbn = isbn;
    this.author = author;
  }

  create(book){
    this.find();
    listBooks.push(book);
    this.write();
    return 'Book saved!';
  }

  find(){
    let exists = fs.existsSync('./storage/book.json');
    if(exists){
      listBooks = JSON.parse(fs.readFileSync('./storage/book.json'));
    }

    return listBooks;
  }

  findByISBN(isbn){
    let position = this.findIntoList('isbn', isbn);
    return listBooks[position];
  }

  findIntoList(key, value){
    this.find();
    let position = listBooks.findIndex(e=>e[key] === value);
    if(position === -1){
      throw new Error("Book not found!");
    }
    return position;
  }

  deleteByISBN(isbn){
    let position = this.findIntoList('isbn', isbn);
    listBooks.splice(position, 1);
    this.write();
    return "Libro eliminado";
  }

  update(isbn, book){
    let position = this.findIntoList('isbn', isbn);
    let keys = Object.keys(book);
    keys.forEach((key) => {
      if(book[key] !== undefined && key != 'isbn'){
        listBooks[position][key] = book[key];
      }
    });

    this.write();
    return "Libro actualizado";
  }

  write(){
    fs.writeFile('./storage/book.json', JSON.stringify(listBooks), (err) =>{
      if(err){
        throw new Error('Book not saved!');
      }
    });
  }
}

module.exports = Book;