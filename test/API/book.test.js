const request = require('supertest');
const app = require('../../app');

describe('Book', () => {
  let server;
  const port = 3000;

  const book = {
    title: 'El moro',
    pages: 32,
    year: 1987,
    isbn: '1233-45-547-D1',
    author: 'Yo'
  };

  beforeAll((done) => {
    server = app.listen(port);
    done();
  });

  afterAll((done) => {
    server.close();
    done();
  });

  test('should get all books', (done) => {
    request(server)
      .get('/api/v1/book/list')
      .then((res)=>{
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual(expect.objectContaining({listaLibros: expect.any(Array)}));
        expect(res.body.listaLibros).toContainEqual({
          title: 'Cien años de soledad',
          pages: 1,
          year: 1900,
          isbn: 'ASDF2342',
          author: 'Epale3'
        });
        done();
      });
  });

  test('should create a book and return a message', (done) => {
    request(server)
      .post('/api/v1/book/create')
      .send(book)
      .then((res)=>{
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual({status: true, message: 'Book saved!'});
        done();
      });
  });
  
  test('should get a book by ISBN', (done) => {
    request(server)
      .get(`/api/v1/book/${book.isbn}`)
      .then((res)=>{
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual({status: true, book});
        done();
      });
  });

  test('should get a error when find a book by ISBN', (done) => {
    request(server)
      .get(`/api/v1/book/${book.isbn}2`)
      .then((res)=>{
        expect(res.statusCode).toEqual(500);
        done();
      });
  });

  test('should delete a book and return a message', (done) => {
    request(server)
      .delete(`/api/v1/book/delete/${book.isbn}`)
      .then((res)=>{
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual({status: true, message: 'Libro eliminado'});
        done();
      });
  });

  test('should update a book and return a message', (done) => {
    let isbn = '123-456-7890-E1';
    request(server)
      .put(`/api/v1/book/update/${isbn}`)
      .send({title: 'La iliada by Ruben2'})
      .then((res)=>{
        expect(res.statusCode).toEqual(200);
        expect(res.body).toEqual({status: true, message: 'Libro actualizado'});
        done();
      });
  });
});